package com.example.rss_205150401111051.adapter;

import android.content.Context;
import android.os.Build;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.example.rss_205150401111051.R;
import com.example.rss_205150401111051.model.Model;
import com.example.rss_205150401111051.volley.MySingleton;

import java.util.ArrayList;

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.ViewHolder> {
    private Context ctx;
    private ArrayList<Model> nList;
    private MySingleton mySingleton;

    public RecyclerAdapter(Context ctx, ArrayList<Model> nList) {
        this.ctx = ctx;
        this.nList = nList;
        mySingleton = MySingleton.getInstance(ctx);
    }

    @NonNull
    @Override
    public RecyclerAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(ctx.getApplicationContext()).inflate(R.layout.list_item,parent,false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Model model = nList.get(position);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            holder.title.setText(Html.fromHtml(model.getTitle(),Html.FROM_HTML_MODE_LEGACY));
            holder.des.setText(Html.fromHtml(model.getDes(),Html.FROM_HTML_MODE_LEGACY));
        }
        holder.category.setText("Category : " + model.getCategory());


    }

    @Override
    public int getItemCount() {
        return nList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{

        private TextView title;
        private TextView des;
        private TextView category;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            title = itemView.findViewById(R.id.titleText);
            des =itemView.findViewById(R.id.detailsText);
            category =itemView.findViewById(R.id.category);

        }
    }
}
